from django.urls import path
from apps.mercadopago.views import nuevaCompra,carrito
app_name ='mercadopago'

urlpatterns = [

    path('nuevaCompra',nuevaCompra, name='nuevaCompra'),
    path('carrito',carrito, name='carrito'),

]