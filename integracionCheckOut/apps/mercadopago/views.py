from django.http import HttpResponse
from django.shortcuts import render
import mercadopago
import json

from django.views.decorators.csrf import csrf_exempt

mp = mercadopago.MP("ACCESS-TOKEN") #ACA TIENE QUE IR EL ACCESS TOKEN DE UNA CUENTA CREADA COMO DE VENDEDOR, EN MI CASO PROBE CON UNA CUENTA REAL PORQUE POR ALGUN MOTIVO NO ENCONTRE LA SECCION DE HACER LA CUENTA PARA VENDEDOR Y
# CON LA CREDENCIAL DEL USUARIO DE TEST NO FUNCIONO.

@csrf_exempt
def nuevaCompra(request, **kwargs):
    if request.method == 'POST':
        total = request.POST.get('total')
        preference = {
            "items": [
                {
                    "title": "Test",
                    "quantity": 1,
                    "currency_id": "ARG",
                    "unit_price": int(total)
                },


            ],
            'back_urls': {'success': 'localhost:8000'}



        }

        preferencia = mp.create_preference(preference)

        print(preferencia)
        # preference_id = preferencia['response']['id']
        preference_id = preferencia['response']['init_point']
        # preference_id = preferencia['response']['sandbox_init_point']
        return HttpResponse(json.dumps(preference_id, indent=4),content_type='application/json')

def index(request):
    return render(request,'base/principal.html')

def carrito(request):
    return render(request,'base/carrito.html')