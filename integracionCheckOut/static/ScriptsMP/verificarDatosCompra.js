
$(document).ready(function(){
    Mercadopago.setPublishableKey('TEST-d0bae774-59f6-45fd-8d4c-ab78d4e82e63')
    $.ajax({
        type:"GET",
        url:"/mercadopago/datosPago",
        success:function(data){
        console.log(data['descripcionProducto'].length)
        var acum = 0
        for(var i=1;i<data['descripcionProducto'].length;i++){
            var att_class = document.createAttribute("class");
            att_class.value ="titulo-2";
            var producto = document.createElement('li');
            producto.setAttributeNode(att_class)
            var descripcion = document.createTextNode(data['descripcionProducto'][i][0]+" "+"");
            var unidades = document.createTextNode(""+'Unidades:'+""+data['descripcionProducto'][i][1])
            producto.appendChild(descripcion);
            producto.appendChild(unidades);
          document.getElementById("miLista").appendChild(producto);
        }
          var att_valor = document.createAttribute("value");
        acum = data['precio']
        var valor_total = acum + acum*(data['porcentaje']/100)
        document.getElementById("total").innerHTML = valor_total;
        document.getElementById("total").setAttribute('value',valor_total)
        if ((data['medio_pago'] == 'credit_card') || (data['medio_pago'] == 'debit_card')){
            if (data['medio_pago'] == 'credit_card'){
            document.getElementById("medio_pago").innerHTML = "Tarjeta de Credito" +" "+ "("+data['nombre_metodo_pago']+")"
            }else{
            document.getElementById("medio_pago").innerHTML = "Tarjeta de Debito" +" "+ "("+data['nombre_metodo_pago']+")"

            }
            document.getElementById("medio_pago").setAttribute('value',data['metodo_pago'])
            document.getElementById("terminacion").innerHTML = "("+"..."+"000"+")"
            cuota = (valor_total / data['cuotas']).toFixed(2)
            document.getElementById("cuotas").innerHTML = data['cuotas'] +" " + "cuotas de"+" " + "$" + cuota
            document.getElementById("cuotas").setAttribute('value',data['cuotas'])
            document.getElementById("shadow").setAttribute('value',data['token'] )
        }else{
            document.getElementById("medio_pago").innerHTML = data['medio_pago']
            document.getElementById("medio_pago").setAttribute('value',data['medio_pago'])
            document.getElementById("terminacion").innerHTML = "Solo Tarjeta"
            cuota = 0
            document.getElementById("cuotas").innerHTML = "Solo Tarjeta"
            document.getElementById("cuotas").setAttribute('value',data['cuotas'])
            document.getElementById("shadow").setAttribute('value',data['token'] )
        }
        }
    });
     function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
    }

 $("#finalizarFlujo").click(function(){
    var token =  getCookie('csrftoken');
    $.ajax({
    type:"POST",
    url:"/mercadopago/crearPago",
    data:{
    "token":document.getElementById('shadow').getAttribute('value'),
    "n_cuotas":parseInt(document.getElementById('cuotas').getAttribute('value')),
    "total":parseInt(document.getElementById('total').getAttribute('value')),
    "payment_method_id":document.getElementById('medio_pago').getAttribute('value'),
    },
    headers: { "X-CSRFToken": token },
    success:function(data){
//        alert(typeof(data))
        if (data == 200) {
                window.location = '/mercadopago/transaccionAprobada';
                }
        else{
              window.location = '/mercadopago/transaccionRechazada'
            }
        }
        });
    });
})